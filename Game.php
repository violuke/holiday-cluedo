<?php

class Game {
    private $gameName;
    private $gameFile;
    private $isNewGame;

    // Game data
    private $players = array();
    private $places = array();
    private $weapons = array();

    private $gameDetails = array();

    const ENCRYPTION_KEY = 'tr3d$QP@F5\g]Xm4E^R.&jPt>=jJ"h';
    const ENCRYPTION_METHOD = 'AES-128-ECB';

    public function __construct($gameName)
    {
        $this->gameName = $gameName;
        $this->gameFile = __DIR__.'/GameData/'.strtolower(str_replace('/', '-', str_replace(' ', '-', $gameName))).'.json';
        $this->isNewGame = !file_exists($this->gameFile);

        // Load data
        if (!$this->isNewGame){
            $this->loadSavedData();
        }
    }

    public function isNewGame(){
        return $this->isNewGame;
    }

    public function getPlayers(){
        return $this->players;
    }

    public function getPlaces(){
        return $this->places;
    }

    public function getWeapons(){
        return $this->weapons;
    }

    private function loadSavedData(){
        $savedData = file_get_contents($this->gameFile);
        $savedData = openssl_decrypt($savedData, self::ENCRYPTION_METHOD, self::ENCRYPTION_KEY);
        $savedData = json_decode($savedData, true);

        $this->players = $savedData['players'];
        $this->places = $savedData['places'];
        $this->weapons = $savedData['weapons'];
        if (isset($savedData['gameDetails'])) {
            $this->gameDetails = $savedData['gameDetails'];
        }
    }

    private function saveData(){
        $savedData = array(
            'players' => $this->players,
            'places' => $this->places,
            'weapons' => $this->weapons,
            'gameDetails' => $this->gameDetails,
        );

        $dataToSave = json_encode($savedData);
        $dataToSave = openssl_encrypt($dataToSave, self::ENCRYPTION_METHOD, self::ENCRYPTION_KEY);

        file_put_contents($this->gameFile, $dataToSave);
    }

    public function addPlayer($playerName){
        $this->players[] = $playerName;
        $this->gameDetails = array(); // Reset build data as it will not longer be correct
        $this->saveData();
    }

    public function addPlace($place){
        $this->places[] = $place;
        $this->gameDetails = array(); // Reset build data as it will not longer be correct
        $this->saveData();
    }

    public function addWeapon($weapon){
        $this->weapons[] = $weapon;
        $this->gameDetails = array(); // Reset build data as it will not longer be correct
        $this->saveData();
    }

    public function buildGame(){
        // Sort players
        $players = $this->players;
        shuffle($players);

        // Make copy of places & weapons
        $places = $this->places;
        $weapons = $this->weapons;

        foreach ($players as $i => $player){
            // Determine random place/weapon
            $randomPlaceKey = array_rand($places);
            $randomWeaponKey = array_rand($weapons);

            $this->gameDetails[] = array(
                'player' => $player,
                'victim' => isset($players[$i + 1]) ? $players[$i + 1] : $players[0],
                'place' => $places[$randomPlaceKey],
                'weapon' => $weapons[$randomWeaponKey],
            );

            // Remove used places/weapons
            unset($places[$randomPlaceKey]);
            unset($weapons[$randomWeaponKey]);
        }

        $this->saveData();
    }

    public function getPlayerDetails($playerName){
        foreach ($this->gameDetails as $details){
            if (strtolower($details['player']) == strtolower($playerName)){
                return $details;
            }
        }

        throw new Exception('Could not find player details for "'.$playerName.'"');
    }

    public function gameAlreadyBuilt(){
        return count($this->gameDetails) > 0;
    }
}