<?php

require('CLI.php');
require('Game.php');

// Prompt for game name
$gameName = CLI::prompt_user_input('Input game name', '/^[^\s]+$/');

// Build game
$game = new Game($gameName);

if ($game->isNewGame()){
    CLI::green('This appears to be a new game.');
} else {
    CLI::light_red('A game already exists with this name. You are continuing that game!');
    CLI::press_any_key_to_continue();
}

// Prompt for new players
while (show_players_and_prompt($game)){
    $playerName = trim(CLI::prompt_user_input('Enter player name', '/.+/'));
    if ($playerName != '' && !in_array($playerName, $game->getPlayers())){
        // Prompt for a place
        while (true){
            $place = strtolower(trim(CLI::prompt_user_input('Enter a place', '/.+/')));

            if (in_array($place, $game->getPlaces())){
                CLI::light_red('This place has already been given. Please try another place.');
            } else {
                // Place is ok
                break;
            }
        }

        // Prompt for a weapon
        while (true){
            $weapon = strtolower(trim(CLI::prompt_user_input('Enter a weapon', '/.+/')));

            if (in_array($weapon, $game->getWeapons())){
                CLI::light_red('This weapon has already been given. Please try another weapon.');
            } else {
                // Weapon is ok
                break;
            }
        }

        // Save
        $game->addPlayer($playerName);
        /** @noinspection PhpUndefinedVariableInspection */
        $game->addPlace($place);
        /** @noinspection PhpUndefinedVariableInspection */
        $game->addWeapon($weapon);
    } else {
        CLI::light_red('Player name was invalid or a duplicate. Skipping.');
        CLI::press_any_key_to_continue();
    }

    system('clear');
}

// Check if all players have been added
if (CLI::prompt_user_input('If all players have been added, type "finalise" to build the game and continue.', ['finalise']) == 'finalise'){
    CLI::draw_heading('Building game...');
    if ($game->gameAlreadyBuilt()){
        CLI::yellow('Game data already built from before.');
    } else {
        $game->buildGame();
        CLI::green('Game data built.');
    }
}

// Allow users to lookup their data
while (true){
    system('clear');
    CLI::draw_heading('View your game details');
    $playerName = trim(CLI::prompt_user_input('Enter your name to view your game details', '/.+/'));

    // Get details
    try {
        $playerDetails = $game->getPlayerDetails($playerName);
    } catch (Exception $e){
        CLI::light_red('Error: ' . $e->getMessage());
        CLI::press_any_key_to_continue('Press enter to try again.');
        continue;
    }

    // Show details
    echo CLI::yellow('Player: ', true);
    CLI::echo_normal($playerDetails['player']);
    echo CLI::yellow('Victim: ', true);
    CLI::echo_normal($playerDetails['victim']);
    echo CLI::yellow('Place: ', true);
    CLI::echo_normal($playerDetails['place']);
    echo CLI::yellow('Weapon: ', true);
    CLI::echo_normal($playerDetails['weapon']);
    
    CLI::press_any_key_to_continue('Once you have read your details, press enter to hide your details ready for the next player.');
}


function show_players_and_prompt(Game $game){
    // List players
    $players = $game->getPlayers();
    CLI::draw_heading('Players');
    if (count($players)) {
        foreach ($players as $i => $playerName) {
            CLI::echo_normal(($i + 1).': '.$playerName);
        }
    } else {
        CLI::echo_normal('No players exist in the game yet');
    }

    return CLI::prompt_user_yes_or_no('Add new player to the game?');
}