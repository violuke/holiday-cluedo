<?php

class CLI {
    public static function draw_option_menu($options) {
        foreach ($options as $option) {
            if (preg_match('/_([A-Za-z0-9]+)_/', $option, $matches)) {
                self::echo_normal(preg_replace('/_([A-Za-z0-9]+)_/', self::green($matches[1], true), $option, 1));
            } else {
                throw new Exception('The option "' . $option . '" does not match the required format.');
            }
        }
        echo "\n";
    }

    public static function draw_heading($string) {
        $len = strlen($string);

        if ($len % 2) {
            $len++;
            $string .= ' ';
        }

        $heading_width = 40;
        if ($heading_width < strlen($string) + 6) {
            $heading_width = strlen($string) + 6;
        }

        $pad_cols = ($heading_width - $len) / 2;

        echo "\n";
        self::echo_or_return_coloured_string('*' . str_repeat('-', $heading_width) . '*', false, self::COLOUR_CYAN, null);
        self::echo_or_return_coloured_string('|' . str_repeat(' ', $pad_cols) . $string . str_repeat(' ', $pad_cols) . '|', false, self::COLOUR_CYAN, null);
        self::echo_or_return_coloured_string('*' . str_repeat('-', $heading_width) . '*', false, self::COLOUR_CYAN, null);
        echo "\n";
    }

    public static function prompt_user_input($prompt_message, $valid_options_array_or_regex) {
        while (true) {
            self::yellow($prompt_message);

            $user_input = trim(fgets(fopen('php://stdin', 'r')));

            if (is_array($valid_options_array_or_regex)) {
                if (in_array($user_input, $valid_options_array_or_regex)) {
                    return $user_input;
                }
            } else {
                if (preg_match($valid_options_array_or_regex, $user_input)) {
                    return $user_input;
                }
            }

            self::light_red('The value you entered is not valid. Please try again.');
        }

        return null;
    }

    public static function prompt_user_yes_or_no($prompt_message)
    {
        return self::prompt_user_input_from_options(
                $prompt_message,
                ['_y_ yes', '_n_ no'],
                false,
                false
            ) == 'y';
    }

    public static function prompt_user_input_from_options($prompt_message, $options, $options_is_key_value = false, $return_value_instead_of_key = false) {
        self::yellow($prompt_message);

        $arrow = '→';

        if ($options_is_key_value) {
            $options_for_prompt = array();
            foreach ($options as $k => $v) {
                $options_for_prompt[] = '_' . $k . '_ ' . $arrow . ' ' . $v;
            }
            $options = $options_for_prompt;
        }

        self::draw_option_menu($options);

        $allowed_inputs = array();
        foreach ($options as $option) {
            if (preg_match('/^_([A-Za-z0-9]+)_(.+)$/', $option, $matches)) {
                if (in_array($matches[1], $allowed_inputs)) {
                    throw new Exception('The input value of "' . $matches[1] . '" on option "' . $option . '" is the same as with another option');
                }
                $allowed_inputs[$matches[1]] = trim(ltrim($matches[2], ' ' . $arrow));
            } else {
                throw new Exception('The option "' . $option . '" does not match the required format.');
            }
        }

        while (true) {
            echo self::yellow('Your selection: ', true);

            $user_input = trim(fgets(fopen('php://stdin', 'r')));

            if (in_array($user_input, array_keys($allowed_inputs))) {
                echo self::green('You chose: ', true);
                self::echo_normal($allowed_inputs[$user_input]);

                if ($return_value_instead_of_key) {
                    return $allowed_inputs[$user_input];
                } else {
                    return $user_input;
                }
            }

            self::light_red('The value you entered is not valid. Please try again.');
        }

        return null;
    }

    public static function prompt_user_input_fn($prompt_message, Callable $test_fn) {
        while (true) {
            self::yellow($prompt_message);

            $user_input = trim(fgets(fopen('php://stdin', 'r')));

            $result = $test_fn($user_input);
            if ($result === true) {
                return $user_input;
            } else {
                self::light_red($result);
            }
        }

        return null;
    }

    const COLOUR_BLACK = 'black';
    const COLOUR_DARK_GRAY = 'dark_gray';
    const COLOUR_BLUE = 'blue';
    const COLOUR_LIGHT_BLUE = 'light_blue';
    const COLOUR_GREEN = 'green';
    const COLOUR_LIGHT_GREEN = 'light_green';
    const COLOUR_MAGENTA = 'magenta';
    const COLOUR_CYAN = 'cyan';
    const COLOUR_LIGHT_CYAN = 'light_cyan';
    const COLOUR_RED = 'red';
    const COLOUR_LIGHT_RED = 'light_red';
    const COLOUR_PURPLE = 'purple';
    const COLOUR_LIGHT_PURPLE = 'light_purple';
    const COLOUR_BROWN = 'brown';
    const COLOUR_YELLOW = 'yellow';
    const COLOUR_LIGHT_GRAY = 'light_gray';
    const COLOUR_WHITE = 'white';

    private static $foreground_colors = array(
        self::COLOUR_BLACK => '0;30',
        self::COLOUR_DARK_GRAY => '1;30',
        self::COLOUR_BLUE => '0;34',
        self::COLOUR_LIGHT_BLUE => '1;34',
        self::COLOUR_GREEN => '0;32',
        self::COLOUR_LIGHT_GREEN => '1;32',
        self::COLOUR_CYAN => '0;36',
        self::COLOUR_LIGHT_CYAN => '1;36',
        self::COLOUR_RED => '0;31',
        self::COLOUR_LIGHT_RED => '1;31',
        self::COLOUR_PURPLE => '0;35',
        self::COLOUR_LIGHT_PURPLE => '1;35',
        self::COLOUR_BROWN => '0;33',
        self::COLOUR_YELLOW => '1;33',
        self::COLOUR_LIGHT_GRAY => '0;37',
        self::COLOUR_WHITE => '1;37',
    );


    private static $background_colors = array(
        self::COLOUR_BLACK => '40',
        self::COLOUR_RED => '41',
        self::COLOUR_GREEN => '42',
        self::COLOUR_YELLOW => '43',
        self::COLOUR_BLUE => '44',
        self::COLOUR_MAGENTA => '45',
        self::COLOUR_CYAN => '46',
        self::COLOUR_LIGHT_GRAY => '47',
    );

    public static function echo_normal($string) {
        echo $string . "\n";
    }

    public static function error_message($str, $return = false) {
        return self::echo_or_return_coloured_string($str, $return, self::$foreground_colors[self::COLOUR_BLACK], self::$background_colors[self::COLOUR_RED]);
    }

    public static function purple($string, $return = false) {
        return self::echo_or_return_coloured_string($string, $return, self::COLOUR_PURPLE);
    }

    public static function light_red($string, $return = false) {
        return self::echo_or_return_coloured_string($string, $return, self::COLOUR_LIGHT_RED);
    }

    public static function green($string, $return = false) {
        return self::echo_or_return_coloured_string($string, $return, self::COLOUR_GREEN);
    }

    public static function yellow($string, $return = false) {
        return self::echo_or_return_coloured_string($string, $return, self::COLOUR_YELLOW);
    }

    public static function cyan($string, $return = false) {
        return self::echo_or_return_coloured_string($string, $return, self::COLOUR_CYAN);
    }

    public static function separator($colour = self::COLOUR_CYAN, $return = false) {
        return self::echo_or_return_coloured_string("\n----------\n", $return, $colour);
    }

    private static function echo_or_return_coloured_string($string, $return = false, $foreground_color = null, $background_color = null) {
        $str = self::get_coloured_string($string, $foreground_color, $background_color);
        if (!$return) {
            echo $str . "\n";
        }
        return $str;
    }

    // Returns colored string
    public static function get_coloured_string($string, $foreground_color = null, $background_color = null) {
        $colored_string = '';

        // Check if given foreground color found
        if (isset(self::$foreground_colors[$foreground_color])) {
            $colored_string .= "\033[" . self::$foreground_colors[$foreground_color] . "m";
        }
        // Check if given background color found
        if (isset(self::$background_colors[$background_color])) {
            $colored_string .= "\033[" . self::$background_colors[$background_color] . "m";
        }

        // Add string and end coloring
        $colored_string .= $string . "\033[0m";

        return $colored_string;
    }

    public static function spinner($frame_idx) {
        echo ['|','/','-','\\'][$frame_idx % 4] . "\r";
    }

    public static function sleep_countdown($seconds) {
        echo 'Sleeping for about ' . $seconds . ' seconds...' . "\n";
        for ($i = 0, $j = $seconds * 10; $i < $j; $i++) {
            usleep(100000);
            echo "\r" . str_pad(number_format((($j - $i) / 10), 1), 5);
        }
        echo "\r";
    }

    public static function press_any_key_to_continue($prompt = 'Press any key to continue...') {
        self::purple($prompt);
        return trim(fgets(STDIN));
    }


    // Table related vars
    private static $table_column_widths = array();
    private static $table_rows_done = 0;

    public static function create_new_table($column_heading_text, $column_widths) {
        // Ensure that the number of headings is the same as the number of column widths
        if (count($column_heading_text) != count($column_widths)) {
            throw new Exception('Cannot draw table, ' . count($column_heading_text) . ' headings were provided, but only ' . count($column_widths) . ' column widths!');
        }

        // Store column widths
        self::$table_column_widths = $column_widths;

        // Draw table heading
        $heading_container_row = '+';
        $heading_text_row = '|';
        foreach ($column_heading_text as $k => $heading) {
            $heading_container_row .= str_repeat('-', $column_widths[$k]) . '+';

            $heading_text_row .= ' ';
            if (strlen($heading) > $column_widths[$k] - 2) {
                $this_heading = substr($heading, 0, $column_widths[$k] - 5) . '...';
            } else {
                $this_heading = $heading;
            }

            $heading_text_row .= self::get_coloured_string($this_heading, self::COLOUR_CYAN) . str_repeat(' ', $column_widths[$k] - 2 - strlen($this_heading));

            // End or columns, or another?
            if ($k == count($column_heading_text) - 1) {
                $heading_text_row .= ' |';
            } else {
                $heading_text_row .= ' +';
            }
        }

        self::echo_normal($heading_container_row);
        self::echo_normal($heading_text_row);
        self::echo_normal($heading_container_row);
    }

    public static function add_row_to_table($row_data, $text_colour = null, $background_colour = null, $zebra_stripe = false) {
        // Ensure we have a table with the same number of columns as this data
        if (count($row_data) != count(self::$table_column_widths)) {
            throw new Exception('Cannot add table row data as the number of columns does not match the table!');
        }

        $table_row = '|';
        foreach ($row_data as $k => $text) {
            // Determine zebra strip colours
            if (self::$table_rows_done % 2 == 0) {
                $zs_text_colour = null;
                $zs_background_colour = null;
            } else {
                $zs_text_colour = self::COLOUR_BLACK;
                $zs_background_colour = self::COLOUR_LIGHT_GRAY;
            }

            $table_row .= ' ';
            if (strlen($text) > self::$table_column_widths[$k] - 2) {
                $this_text = substr($text, 0, self::$table_column_widths[$k] - 5) . '...';
            } else {
                $this_text = $text;
            }

            // Zebra striping will override the colours set manually
            if ($zebra_stripe) {
                $table_row .= self::get_coloured_string(
                        $this_text,
                        $text_colour === null ? $zs_text_colour : $text_colour,
                        $background_colour === null ? $zs_background_colour : $background_colour
                    ) . self::get_coloured_string(str_repeat(' ', self::$table_column_widths[$k] - 2 - strlen($this_text)), null, $zs_background_colour);
            } else {
                $table_row .= self::get_coloured_string($this_text, $text_colour, $background_colour) . str_repeat(' ', self::$table_column_widths[$k] - 2 - Vio_Util_UTF8Strings::strlen($this_text));
            }

            $table_row .= ' |';
        }

        self::$table_rows_done++;

        self::echo_normal($table_row);
    }

    public static function finish_table() {
        $row = '+';
        foreach (self::$table_column_widths as $width) {
            $row .= str_repeat('-', $width) . '+';
        }

        self::echo_normal($row);

        self::$table_column_widths = array();
    }

    public static function beep()
    {
        echo "\x07";
    }
}